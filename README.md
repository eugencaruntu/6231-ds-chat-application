# Practice with Socket Programming

In this assignment, you will code a group chat room. In this chat room, clients connect to a remote server with a designated IP and port number. The server accepts connections from an arbitrary number of clients. Any message sent from one client is broadcast to all other clients. You must leverage the concepts of socket programming and multi-threading to achieve this task. Also, create a GUI for your chat room.

## Remember 

Use the concepts of socket programming and multi-threading.

## Running the server and client applications

Build with Maven from the root of the project: `mvn clean install`.
Use the fat jar files with dependencies o execute the applications as described below.

#### Running from a bash script

A script is provided for simplicity. One server and 3 clients will run having their logs in same window. Adjust the script as needed: 
`./run.sh
`
#### Running JavaFX with JRE 11
JavaFX is not part of JRE 11 anymore. To run each application you need:

`
java --module-path lib/ --add-modules=javafx.controls -jar server-1.0-SNAPSHOT-jar-with-dependencies.jar
`

`
java --module-path lib/ --add-modules=javafx.controls -jar client-1.0-SNAPSHOT-jar-with-dependencies.jar
`

If run with JRE 8, consider adjusting above commands.

## Released Features

#### Server start / stop screen
- Minimum validation on port field is implemented (port must be between 1001 and 65536 inclusively).

![Server Start/Stop](img/chat-server-screen-start-stop.png)

#### Client Login Screen
- Minimum validation on fields is implemented (port must be between 1001 and 65536 inclusively, other fields should be at least one character)
- Chat screen will launch only after the client was able to connect

![Client Login](img/chat-client-connection-screen.png)


#### Client Chat Session with 2 users connected
- Connected user list is updated through server/client messaging
- Users disconnecting will notify the chat server

![Client Chat Sessions](img/chat-2-users-chat-screen.png)


#### When server is stopped, clients need to reconnect
- Stopping the server will ntify clients and gracefully handle the connection closing
- Users will be disconnected and sending messages is disables
- Users must reconnect once server is available and then are able to resume the chat
- Reconnection is being validated and only if client was successfully connected will resume the chat

![Reconnect Client](img/chat-client-disconnected-reconnect-listener.png)

#### Both the server and client will log their activity on console
- There is one console for client and one for server when run separately. (when run from same terminal they will log on same console)

![Activity Logs](img/chat-logs.png)




package lab2.comp6231.org;/* Eugen Caruntu, ID 29077103, COMP 6231 LabAssignment 2 */

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * JavaFX application for server side
 */
public class ServerApplication extends Application {
    private Server server;
    private Thread serverThread;
    private Button start_button;
    private Button stop_button;
    private TextField serverPort_box;
    private javafx.beans.property.StringProperty title = new SimpleStringProperty("ChatSever [stopped]");
    private javafx.beans.property.SimpleBooleanProperty serverRunning = new SimpleBooleanProperty(false);

    /**
     * Create the server screen
     *
     * @return the scene
     */
    private Scene startScene() {
        GridPane main = new GridPane();
        main.getStylesheets().add(getClass().getResource("/main.css").toExternalForm());
        Label serverPort_label = new Label("Server Port:");
        serverPort_box = new TextField("8080");
        start_button = new Button("Start");
        stop_button = new Button("Stop");
        stop_button.setOnAction(Event -> stopServer());
        stop_button.setVisible(false);

        addListeners();

        main.add(serverPort_label, 0, 0);
        main.add(serverPort_box, 0, 1);
        main.add(start_button, 1, 1);
        main.add(stop_button, 1, 1);

        return new Scene(main, 300, 100);
    }

    /**
     * Sets listeners and actions
     */
    private void addListeners() {
        serverRunning.addListener((observable, oldValue, newValue) -> {
            start_button.setVisible(!serverRunning.getValue());
            stop_button.setVisible(serverRunning.getValue());
            if (serverRunning.getValue()) {
                title.setValue("ChatSever [running]");
            } else {
                title.setValue("ChatSever [stopped]");
            }
        });

        start_button.setOnAction(Event -> {
            try {
                int port = Integer.parseInt(serverPort_box.getText());
                if (1000 < port && port < 65536) {
                    server = Server.getInstance(Integer.parseInt(serverPort_box.getText()));
                    serverThread = new Thread(server::setRunning);
                    serverThread.setDaemon(true);
                    serverThread.start();
                    serverRunning.setValue(true);
                    server.running.addListener((observable, oldValue, newValue) -> Platform.runLater(() -> serverRunning.setValue(server.running.getValue())));
                } else {
                    serverPort_box.setText("1000 < port < 65536");
                }
            } catch (NumberFormatException e) {
                Server.LOGGER.error("Invalid port entered. Must be a number between 1000 and 65536");
            }
        });
    }

    /**
     * Stops the server and its thread
     */
    private void stopServer() {
        server.stop();
        serverThread.interrupt();
    }

    /**
     * Start the application
     *
     * @param stage the root stage
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle(title.getValue());
        title.addListener((observable, oldValue, newValue) -> stage.setTitle(newValue));
        stage.setScene(startScene());
        stage.show();
    }

    /**
     * Stop the application
     */
    @Override
    public void stop() {
        try {
            super.stop();
            if (server != null) {
                server.stop();
            }

        } catch (Exception e) {
            Server.LOGGER.error(e);
        }
    }

    /**
     * The main entry for the application
     *
     * @param args none expected
     */
    public static void main(String[] args) {
        launch();
    }

}

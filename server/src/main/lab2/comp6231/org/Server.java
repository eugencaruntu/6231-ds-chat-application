package lab2.comp6231.org;/* Eugen Caruntu, ID 29077103, COMP 6231 LabAssignment 2 */

import javafx.beans.property.SimpleBooleanProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The server class
 */
class Server {
    private static Server SERVER_INSTANCE = null;
    private static int counter;
    javafx.beans.property.SimpleBooleanProperty running = new SimpleBooleanProperty(false);
    private ServerSocket serverSocket;
    private static Set<ClientThread> clientThreads;
    private static HashMap<Integer, String> clientNames;

    static final Logger LOGGER = LogManager.getLogger(lab2.comp6231.org.Server.class);

    /**
     * Getter for the server instance
     *
     * @return the server instance
     */
    static Server getInstance(int port) {
        if (SERVER_INSTANCE == null) {
            SERVER_INSTANCE = new Server(port);
        }
        return SERVER_INSTANCE;
    }

    /**
     * Accessor for clientThreads
     *
     * @return the set of active threads
     */
    static Set<ClientThread> getClients() {
        return clientThreads;
    }

    /**
     * Private constructor for the singleton server
     */
    private Server(int port) {
        try {
            serverSocket = new ServerSocket(port);
            LOGGER.info("Server is listening at port " + port);
            clientThreads = ConcurrentHashMap.newKeySet();
            clientNames = new HashMap<>();
        } catch (IOException e) {
            stop();
            LOGGER.error(e);
        }
    }

    /**
     * Disconnect client threads before shutting down the server
     */
    void stop() {
        String message = "Chat room is shutting down...";
        try {
            sendMessage(message, "SERVER");
            for (ClientThread clientThread : clientThreads) {
                clientThread.disconnectClient();
            }
            serverSocket.close();
            if (running.getValue()) running.setValue(false);
            SERVER_INSTANCE = null;
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Send the user list within a message
     */
    synchronized static void sendUserList() {
        sendMessage(clientNames.toString(), "USERLIST");
    }

    /**
     * Send the message to all other clients.
     * Use a new thread each time
     *
     * @param message the message to send
     * @param from    the client could be used to target specific clients
     */
    static synchronized void sendMessage(String message, String from) {
        Thread sending = new Thread(() -> {
            for (ClientThread clientThread : clientThreads) {
                try {
                    LOGGER.info("[SERVER] sends to user ID [" + clientThread + "] the message from [" + from + "]: " + message);
                    clientThread.outputStream.writeUTF(from + ": " + message);
                    clientThread.outputStream.flush();
                } catch (IOException e) {
                    LOGGER.error(e);
                }
            }
        });
        sending.start();
    }

    /**
     * Add the client ID and name to the collection
     *
     * @param clientID   the client ID unique key assigned by the server
     * @param clientName the name of the client
     */
    synchronized static void addClientNames(int clientID, String clientName) {
        clientNames.put(clientID, clientName);
    }

    /**
     * Remove the client name from the collection
     *
     * @param clientID the clientID key
     */
    synchronized static void removeClientName(int clientID) {
        clientNames.remove(clientID);
    }

    /**
     * Start the server and listen. Handle each client in a new thread.
     * Stop the server if exception occurs
     */
    void setRunning() {
        running.setValue(true);
        try {
            while (running.getValue()) {
                Socket clientSocket = serverSocket.accept();
                ClientThread clientThread = new ClientThread((++counter), clientSocket);
                Thread thread = new Thread(clientThread);
                clientThreads.add(clientThread);
                thread.start();
            }
            stop();
        } catch (IOException e) {
            stop();
            LOGGER.error(e);
        }
    }
}

/**
 * A helper class to run client threads
 */
class ClientThread implements Runnable {
    private int clientID;
    private String clientName;
    private boolean clientConnected = false;
    private Socket clientSocket;
    private DataInputStream inputStream;
    DataOutputStream outputStream;

    private static final Logger LOGGER = LogManager.getLogger(ClientThread.class);

    /**
     * A client thread initialization with in/out streams
     *
     * @param clientID the unique ID for this client assigned by the server
     * @param socket   the socket used by the client
     */
    ClientThread(int clientID, Socket socket) {
        this.clientID = clientID;
        clientSocket = socket;
        try {
            inputStream = new DataInputStream(clientSocket.getInputStream());
            outputStream = new DataOutputStream(clientSocket.getOutputStream());
            clientConnected = true;
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Authenticate a client by retrieving its name and adding to the collection
     */
    private void authenticateClient() {
        try {
            String receivedMessage = inputStream.readUTF();
            if (receivedMessage.contains("Hello, my name is @")) {
                clientName = receivedMessage.split("@")[1];
                Server.addClientNames(clientID, clientName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Disconnect a client thread by removing it from collections and closing the socket
     */
    void disconnectClient() {
        try {
            clientConnected = false;
            Server.getClients().remove(this);
            Server.removeClientName(clientID);
            Server.sendUserList();
            LOGGER.warn("The client ID [" + clientID + "] is being disconnected by the server.");
            clientSocket.close();
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Run the client thread: authenticate, receive/send as long as is connected
     */
    @Override
    public void run() {
        try {
            while (clientName == null) {
                authenticateClient();
            }
            Server.sendUserList();
            while (clientConnected) {
                String receivedMessage = inputStream.readUTF();
                LOGGER.info("[SERVER] receives from [" + clientName + "]: " + receivedMessage);
                Server.sendMessage(receivedMessage, clientName);
            }
        } catch (IOException e) {
//            LOGGER.error(e); // gracefully handle with a disconnect instead of logging the error
            disconnectClient();
        }
    }

}
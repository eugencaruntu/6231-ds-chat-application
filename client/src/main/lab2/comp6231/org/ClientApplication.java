package lab2.comp6231.org;/* Eugen Caruntu, ID 29077103, COMP 6231 LabAssignment 2 */

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.List;

/**
 * JavaFX application for client side
 */
public class ClientApplication extends Application {
    private Client client;
    private String userName;
    private String host;
    private int port;
    private Button reconnect_button;
    private Button send_button;
    private TextField sendMessage_box;
    private ListView<String> chatArea_box;
    private javafx.beans.property.StringProperty title = new SimpleStringProperty("ChatBox [disconnected]");

    private ObservableList<String> receivedMessages = FXCollections.observableArrayList();
    private ObservableList<String> users = FXCollections.observableArrayList();
    private ListView<String> connectedUsers_list = new ListView<>();

    /**
     * Make the initial login screen
     *
     * @param rootStage the main stage
     * @return the login scene
     */
    private Scene logInScreen(Stage rootStage) {
        GridPane main = new GridPane();
        main.getStylesheets().add(getClass().getResource("/main.css").toExternalForm());
        Label clientID_label = new Label("User Name:");
        TextField clientID_box = new TextField("John");
        Label serverHost_label = new Label("Server Address:");
        TextField serverHost_box = new TextField("127.0.0.1");
        Label serverPort_label = new Label("Server Port:");
        TextField serverPort_box = new TextField("8080");
        Button connect_button = new Button("Connect");

        connect_button.setOnAction(Event -> {
            if (validateEntries(clientID_box, serverHost_box, serverPort_box)) {
                startChat(rootStage);
            }
        });

        main.add(clientID_label, 0, 0);
        main.add(clientID_box, 1, 0);
        main.add(serverHost_label, 0, 1);
        main.add(serverHost_box, 1, 1);
        main.add(serverPort_label, 0, 2);
        main.add(serverPort_box, 1, 2);
        main.add(connect_button, 1, 4);

        return new Scene(main, 350, 200);
    }

    /**
     * Minimal validation on entries
     *
     * @param clientID_box   the client name needs to be at least one character
     * @param serverHost_box should be longer than one character
     * @param serverPort_box the port should be greater than 1000
     * @return true if all are validated, false otherwise
     */
    private boolean validateEntries(TextField clientID_box, TextField serverHost_box, TextField serverPort_box) {
        userName = clientID_box.getText();
        host = serverHost_box.getText();
        port = Integer.parseInt(serverPort_box.getText());
        try {
            int port = Integer.parseInt(serverPort_box.getText());
            if (1000 < port && port < 65536) {
                return userName.length() > 0 && host.length() > 0;
            } else {
                serverPort_box.setText("1000 < port < 65536");
            }
        } catch (NumberFormatException e) {
            Client.LOGGER.error("Invalid port entered. Must be a number between 1000 and 65536");
        }
        return false;
    }

    /**
     * Make the chat scene
     *
     * @param rootStage the root stage
     * @return the chat scene
     */
    private Scene chatScreen(Stage rootStage) {
        GridPane main = new GridPane();
        main.getStylesheets().add(getClass().getResource("/main.css").toExternalForm());

        connectedUsers_list.getStylesheets().add(getClass().getResource("/userList.css").toExternalForm());
        connectedUsers_list.setEditable(false);
        connectedUsers_list.setItems(users);

        chatArea_box = new ListView<>();
        chatArea_box.setEditable(false);
        chatArea_box.setMinSize(450, 520);
        chatArea_box.setItems(receivedMessages);

        sendMessage_box = new TextField();
        sendMessage_box.setOnAction(event -> sendMessage());

        send_button = new Button("Send");
        send_button.setOnAction(event -> sendMessage());

        reconnect_button = new Button("Reconnect");
        reconnect_button.setVisible(false);
        reconnect_button.setOnAction(event -> connect(rootStage));

        main.add(chatArea_box, 0, 0);
        main.add(sendMessage_box, 0, 1);
        main.add(connectedUsers_list, 1, 0);
        main.add(send_button, 1, 1);
        main.add(reconnect_button, 1, 1);

        return new Scene(main, 650, 600);
    }

    /**
     * Make a client and start the chat only if client is connected
     *
     * @param loginStage the stage
     */
    private void startChat(Stage loginStage) {
        Thread clientThread = new Thread(() -> {
            client = new Client(userName, host, port);
            client.connect();
            if (client.connected.getValue()) {
                Platform.runLater(() -> {
                    title.setValue("ChatBox [" + userName + "]");
                    addListeners(loginStage);
                    loginStage.close();
                    loginStage.setScene(chatScreen(loginStage));
                    loginStage.show();
                });
            }
        });
        clientThread.setDaemon(true);
        clientThread.start();
    }

    /**
     * Update the messages on the chat box, jump to last one
     */
    private void updateChat() {
        Platform.runLater(() -> {
            receivedMessages.add(client.getLastMessageReceived().getValue());
            chatArea_box.scrollTo(receivedMessages.size());
        });
    }

    /**
     * Update the list of users if a new one is received
     */
    private void updateUsers() {
        Platform.runLater(() -> {
            users.clear();
            List<String> list = client.getConnectedUsers();
            users.add(list.size() + " USERS ONLINE:");
            users.addAll(list);
        });
    }

    /**
     * Send a message if its length is not zero
     */
    private void sendMessage() {
        String message = sendMessage_box.getText();
        if (message.length() >= 1) {
            client.send(message);
            sendMessage_box.clear();
        }
    }

    /**
     * Add listeners
     *
     * @param rootStage the root stage where header is updated
     */
    private void addListeners(Stage rootStage) {
        client.connected.addListener((observable, oldValue, newValue) -> Platform.runLater(() -> {
            reconnect_button.setVisible(!newValue);
            send_button.setVisible(newValue);
            sendMessage_box.setDisable(!newValue);
        }));
        receivedMessages.addListener((ListChangeListener.Change<? extends String> c) -> {
            if (c.toString().contains("SERVER: Chat room is shutting down")) {
                disableControls();
            }
        });
        client.getConnectedUsers().addListener((ListChangeListener.Change<? extends String> c) -> updateUsers());
        client.getLastMessageReceived().addListener((observable, oldValue, newValue) -> updateChat());
        title.addListener((observable, oldValue, newValue) -> rootStage.setTitle(newValue));

    }

    /**
     * Disable controls when disconnected
     */
    private void disableControls() {
        title.setValue("ChatBox [disconnected]");
        reconnect_button.setVisible(true);
        send_button.setVisible(false);
        sendMessage_box.setDisable(true);
    }

    /**
     * Attempt to reconnect and set controls active if connected
     *
     * @param rootStage the root stage
     */
    private void connect(Stage rootStage) {
        Thread clientThread = new Thread(() -> {
            client = new Client(userName, host, port);
            client.connect();
            Platform.runLater(() -> {
                if (client.connected.getValue()) {
                    title.setValue("ChatBox [" + userName + "]");
                    reconnect_button.setVisible(false);
                    send_button.setVisible(true);
                    sendMessage_box.setDisable(false);
                    addListeners(rootStage);
                }
            });
        });
        clientThread.setDaemon(true);
        clientThread.start();
    }

    /**
     * Start the application
     *
     * @param rootStage the root stage
     */
    @Override
    public void start(Stage rootStage) {
        rootStage.setTitle(title.getValue());
        rootStage.setScene(logInScreen(rootStage));
        title.addListener((observable, oldValue, newValue) -> rootStage.setTitle(newValue));
        rootStage.show();
    }

    /**
     * Stop the application
     *
     * @throws Exception if super can't stop
     */
    @Override
    public void stop() throws Exception {
        super.stop();
        if (client != null) {
            client.disconnect();
        }
    }

    /**
     * Entry method of the application
     *
     * @param args none expected
     */
    public static void main(String[] args) {
        launch();
    }

}

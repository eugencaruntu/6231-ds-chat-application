package lab2.comp6231.org;/* Eugen Caruntu, ID 29077103, COMP 6231 LabAssignment 2 */

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

/**
 * The client class
 */
class Client {
    private String name;
    private int serverPort;
    private String serverAddress;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    javafx.beans.property.SimpleBooleanProperty connected = new SimpleBooleanProperty(false);
    private javafx.beans.property.StringProperty lastMessageReceived;
    private ObservableList<String> connectedUsers;

    static final Logger LOGGER = LogManager.getLogger(lab2.comp6231.org.Client.class);

    /**
     * Client constructor
     *
     * @param clientName the name of this client
     * @param host       the server host
     * @param port       the port where the server is listening
     */
    Client(String clientName, String host, int port) {
        name = clientName;
        serverAddress = host;
        serverPort = port;
        lastMessageReceived = new SimpleStringProperty();
        connectedUsers = FXCollections.observableArrayList();
    }

    /**
     * Open a socket and start receiving
     */
    void connect() {
        try {
            Socket clientSocket = new Socket(InetAddress.getByName(serverAddress), serverPort);
            inputStream = new DataInputStream(clientSocket.getInputStream());
            outputStream = new DataOutputStream(clientSocket.getOutputStream());
            connected.setValue(true);
            receive();
            authenticate();
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Send an initial message with the client name
     */
    private void authenticate() {
        send("Hello, my name is @" + name);
    }

    /**
     * Disconnect and send a note if connected
     */
    void disconnect() {
        if (connected.getValue()) {
            send("Goodbye!");
            connected.setValue(false);
        }
    }

    /**
     * Receive and handle messages as long as client is connected.
     * Use a new thread each time.
     */
    private void receive() {
        Thread receiving = new Thread(() -> {
            while (connected.getValue()) {
                try {
                    String message = inputStream.readUTF();
                    LOGGER.info("[" + name + "] receives: " + message);
                    if (message.contains("USERLIST")) {
                        LOGGER.warn("[" + name + "] receives the updated user list.");
                        updateUsers(message);
                    } else if (message.contains("SERVER: Chat room is shutting down")) {
                        lastMessageReceived.setValue(message);
                        LOGGER.warn("[" + name + "] needs to gracefully disconnect now.");
                        disconnect();
                    } else {
                        lastMessageReceived.setValue(message);
                    }
                } catch (IOException e) {
                    disconnect();
                    LOGGER.error(e);
                }
            }
        });
        receiving.start();
    }

    /**
     * Send a message within a new thread
     *
     * @param message the string to be sent
     */
    void send(String message) {
        Thread sending = new Thread(() -> {
            try {
                LOGGER.info("[" + name + "] sends: " + message);
                outputStream.writeUTF(message);
                outputStream.flush();
            } catch (IOException e) {
                LOGGER.error(e);
            }
        });
        sending.start();
    }

    /**
     * Update the collections of connected users
     *
     * @param message the message received containing the user list
     */
    private void updateUsers(String message) {
        String users = message.replace("{", "").replace("}", "").split(":")[1];
        List<String> userList = Arrays.asList(users.replaceAll("(\\d+?=)", "").split(","));
        connectedUsers.clear();
        connectedUsers.addAll(userList);
    }

    /**
     * Retrieve the last message received, observable by the view
     *
     * @return the last message that was received
     */
    StringProperty getLastMessageReceived() {
        return lastMessageReceived;
    }

    /**
     * Retrieve the connected users, observable by the view
     *
     * @return the list of users
     */
    ObservableList<String> getConnectedUsers() {
        return connectedUsers;
    }

}
